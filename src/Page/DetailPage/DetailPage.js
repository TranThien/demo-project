import React, { useEffect, useState } from "react";
import { useParams } from "react-router-dom";
import Header from "../../Component/Header/Header";
import { getItemFilm } from "../../service/movieServices";

export default function DetailPage() {
  const detail = useParams();

  const [detailFilm, setDetailFilm] = useState({});

  useEffect(() => {
    getItemFilm(detail.maPhim)
      .then((film) => {
        setDetailFilm(film.data.content);
      })
      .catch((error) => {});
  }, [detail]);

  return (
    <div>
      <h1>{detailFilm.tenPhim}</h1>
    </div>
  );
}
