import { list } from "postcss";
import React, { useEffect, useState } from "react";
import { getMovieList } from "../../service/movieServices";
import MovieList from "./MovieList/MovieList";
import MovieTab from "./MovieTab/MovieTab";

export default function HomePage() {
  const [film, setFilm] = useState([]);
  useEffect(() => {
    getMovieList()
      .then((res) => {
        setFilm(res.data.content);
      })
      .catch((err) => {});
  }, []);

  return (
    <div className="container mx-auto">
      <div className="">
        <MovieList data={film} />
      </div>
    </div>
  );
}
