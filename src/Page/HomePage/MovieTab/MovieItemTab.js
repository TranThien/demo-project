import React from "react";
import moment from "moment";
// chuyển đổi sang tiếng Vi
// import "moment/locale/vi";
// moment.locale("vi");
export default function MovieItemTab(props) {
  const movie = props.movie;
  return (
    <div className="flex mt-10 space-x-5">
      <img className="h-40 w-28 object-cover" src={movie.hinhAnh} alt="" />
      <div>
        <h3 className="font-medium text-2xl">{movie.tenPhim}</h3>
        <div className="grid grid-cols-3  gap-7">
          {movie.lstLichChieuTheoPhim.slice(0, 6).map((lichChieu, index) => {
            return (
              <p
                key={index}
                className="bg-red-500 text-white px-3 py-2 rounded"
              >
                {moment(lichChieu.ngayChieuGioChieu).format("LLL")}
              </p>
            );
          })}
        </div>
      </div>
    </div>
  );
}
/*
{
    "lstLichChieuTheoPhim": [
        {
            "maLichChieu": 44349,
            "maRap": "529",
            "tenRap": "Rạp 9",
            "ngayChieuGioChieu": "2021-10-03T03:17:38",
            "giaVe": 75000
        }
    ],
    "maPhim": 8301,
    "tenPhim": "Đạo mộ bút ký: Trùng Khởi ",
    "hinhAnh": "https://movienew.cybersoft.edu.vn/hinhanh/dao-mo-but-ky-trung-khoi_gp01.jpg",
    "hot": true,
    "dangChieu": true,
    "sapChieu": false
}*/
