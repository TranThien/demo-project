import React, { useEffect, useState } from "react";
import { getMovieByTheater } from "../../../service/movieServices";
import { Tabs } from "antd";
import MovieItemTab from "./MovieItemTab";
const onChange = (key) => {};

export default function MovieTab() {
  const [dataTheater, setDataTheater] = useState([]);
  //render cụm rạp
  const renderHeThongRap = () => {
    return dataTheater.map((heThongRap) => {
      return {
        label: <img src={heThongRap.logo} alt="" className="w-16 h-16" />,
        key: heThongRap.maHeThongRap,
        children: (
          <Tabs
            style={{ height: 500 }}
            tabPosition="left"
            defaultActiveKey="1"
            onChange={onChange}
            //render
            items={renderListCumRap(heThongRap)}
          />
        ),
      };
    });
  };
  // render list cụm rạp
  const renderListCumRap = (heThongRap) => {
    return heThongRap.lstCumRap.map((cumRap) => {
      return {
        label: (
          <div className="w-40">
            <h5>{cumRap.tenCumRap}</h5>
            <p className="truncate">{cumRap.diaChi}</p>
          </div>
        ),

        key: cumRap.maCumRap,
        children: (
          <div style={{ height: 400, overflowY: "scroll" }}>
            {renderRap(cumRap)}
          </div>
        ),
      };
    });
  };
  //render từng rap phim
  const renderRap = (cumRap) => {
    return cumRap.danhSachPhim.map((movie, index) => {
      return <MovieItemTab movie={movie} key={index} />;
    });
  };
  useEffect(() => {
    getMovieByTheater().then((item) => {
      setDataTheater(item.data.content);
    });
  }, []);
  return (
    <div>
      <Tabs
        style={{ height: 500 }}
        tabPosition="left"
        defaultActiveKey="1"
        onChange={onChange}
        //render
        items={renderHeThongRap()}
      />
    </div>
  );
}
