import React from "react";
import { Button, Checkbox, Form, Input, message } from "antd";
import { postLogin } from "../../service/userServices";
import { useNavigate } from "react-router-dom";
import { useDispatch } from "react-redux";
import { SET_USER_LOGIN } from "../../redux/constant/userContant";
import { userLocalStorage, USER_LOCAL } from "../../service/localServices";
import Lottie from "lottie-react";
import bg_animation from "../../assets/957-flight-icon-interaction.json";
import {
  setUserAction,
  setUserActionService,
} from "../../redux/action/userAction";

export default function LoginPage() {
  // navigate dùng để chuyển hướng trang
  let navigate = useNavigate();
  // tải dữ liệu từ redux
  let dispatch = useDispatch();
  // redux thunk
  const onFinishReduxThunk = (value) => {
    // chuyển hướng trang khi dùng redux thunk
    const handleNavigate = () => {
      setTimeout(() => {
        navigate("/");
      }, 1500);
    };
    dispatch(setUserActionService(value, handleNavigate));
  };
  const onFinish = (values) => {
    console.log("Success:", values);
    //lấy api từ backend
    postLogin(values)
      .then((res) => {
        // window.location.pathname = "/";(c2)
        // mess đăng nhập thành công
        message.success("Login successful");

        // dispatch user login
        // dispatch({
        //   type: SET_USER_LOGIN,
        //   payload: res.data.content,
        // });
        dispatch(setUserAction(res.data.content));
        //lưu xuống localStorage
        userLocalStorage.set(res.data.content);
        // đăng nhập thành công chuyển hướng đến trang chủ
        setTimeout(() => {
          //   window.location.href = "/";
          navigate("/");
        }, 1500);
      })
      .catch((err) => {
        message.error("Login fail");
      });
  };
  const onFinishFailed = (errorInfo) => {
    console.log("Failed:", errorInfo);
  };
  return (
    <div className=" w-screen h-screen flex justify-center items-center bg-blue-500">
      <div className="container p-5 rounded bg-white flex ">
        <div className="w-1/2">
          <Lottie animationData={bg_animation} loop={true} />;
        </div>
        <div className="w-1/2">
          <Form
            layout="vertical"
            name="basic"
            labelCol={{
              span: 8,
            }}
            wrapperCol={{
              span: 24,
            }}
            initialValues={{
              remember: true,
            }}
            // onFinish={onFinish}
            onFinish={onFinishReduxThunk}
            onFinishFailed={onFinishFailed}
            autoComplete="off"
          >
            <Form.Item
              label="Username"
              name="taiKhoan"
              rules={[
                {
                  required: true,
                  message: "Please input your username!",
                },
              ]}
            >
              <Input />
            </Form.Item>

            <Form.Item
              label="Password"
              name="matKhau"
              rules={[
                {
                  required: true,
                  message: "Please input your password!",
                },
              ]}
            >
              <Input.Password />
            </Form.Item>
            {/* 
          <Form.Item
            name="remember"
            valuePropName="checked"
            wrapperCol={{
              offset: 8,
              span: 24,
            }}
          >
            <Checkbox>Remember me</Checkbox>
          </Form.Item> */}

            <Form.Item
              wrapperCol={{
                span: 24,
              }}
              className="text-center"
            >
              <Button
                className="bg-blue-500 hover:text-white"
                htmlType="submit"
              >
                Submit
              </Button>
            </Form.Item>
          </Form>
        </div>
      </div>
    </div>
  );
}
