import React from "react";

export default function NotFoundPage() {
  return (
    <div className="h-screen w-screen flex justify-center  items-center">
      <h1 className="animate-bounce text-5xl text-center font-black text-red-600">
        NotFound 404
      </h1>
    </div>
  );
}
