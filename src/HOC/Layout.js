import React from "react";
import Footer from "../Component/Footer/Footer";
import Header from "../Component/Header/Header";

export default function Layout(props) {
  // HOC có props children
  console.log(props);
  return (
    <div>
      <Header />
      {props.children}
      <Footer />
    </div>
  );
}
