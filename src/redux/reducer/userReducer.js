import { userLocalStorage } from "../../service/localServices";
import { SET_USER_LOGIN } from "../constant/userContant";

const initialState = {
  //   user: null, lúc đầu user chưa có dữ liệu . khi có dự liệu thì set lại
  user: userLocalStorage.get(),
};

export const userReducer = (state = initialState, { type, payload }) => {
  switch (type) {
    case SET_USER_LOGIN:
      //state.user = payload
      return { ...state, user: payload };

    default:
      return { ...state };
  }
};
