import { message } from "antd";
import { postLogin } from "../../service/userServices";
import { SET_USER_LOGIN } from "../constant/userContant";

export const setUserAction = (value) => {
  return {
    type: SET_USER_LOGIN,
    payload: value,
  };
};

export const setUserActionService = (value, onSuccess) => {
  return (dispatch) => {
    postLogin(value)
      .then((res) => {
        console.log(res);
        message.success("Đăng nhập thành công");
        //sau khi thành công dispatch lên redux
        dispatch({
          type: SET_USER_LOGIN,
          payload: res.data.content,
        });
        // khi thành công thì gọi lại hàm này
        onSuccess();
      })
      .catch((err) => {
        console.log(err);
      });
  };
};
