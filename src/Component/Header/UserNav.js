import React, { Fragment } from "react";
import { useSelector } from "react-redux";
import { userLocalStorage } from "../../service/localServices";

export default function UserNav() {
  // khi bấm đăng xuất -> trang chủ
  const handleLogout = () => {
    userLocalStorage.remove();
    //
    window.location.href = "/";
  };
  // khi bấm đăng nhập
  const handleLogin = () => {
    window.location.href = "/login";
  };
  // khi bấm đăng ký
  const handleRegister = () => {
    window.location.href = "/register";
  };
  const renderContent = () => {
    if (user) {
      // đăng nhập thành công
      return (
        <Fragment>
          <span>{user?.hoTen}</span>
          <button
            onClick={handleLogout}
            className="border-2 border-black px- py-2 rounded"
          >
            Đăng xuất
          </button>
        </Fragment>
      );
    } else {
      return (
        <Fragment>
          <button onClick={handleRegister}>Đăng ký</button>
          <button onClick={handleLogin}>Đăng nhập</button>
        </Fragment>
      );
    }
  };
  // lấy dữ liệu xuống
  let user = useSelector((state) => {
    return state.userReducer.user;
  });
  console.log(user);
  return <div>{renderContent()}</div>;
}
