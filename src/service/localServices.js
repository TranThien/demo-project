export const USER_LOCAL = "USER_LOCAL";

export const userLocalStorage = {
  // lấy lên từ local
  get: () => {
    let userJson = localStorage.getItem(USER_LOCAL);
    if (userJson) {
      return JSON.parse(userJson);
    } else {
      return null;
    }
  },
  // lưu xuống local
  set: (userData) => {
    let userJson = JSON.stringify(userData);
    localStorage.setItem(USER_LOCAL, userJson);
  },
  remove: () => {
    localStorage.removeItem(USER_LOCAL);
  },
};
