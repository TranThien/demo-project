import { https } from "./configURL";

//lấy danh sách phim
export const getMovieList = () => {
  return https.get("/api/QuanLyPhim/LayDanhSachPhim?maNhom=GP05");
};
//lấy danh sách từng phim
export const getItemFilm = (data) => {
  return https.get(`/api/QuanLyPhim/LayThongTinPhim?MaPhim=${data}`);
};
//lấy thông tin lịch chiếu của rạp
export const getMovieByTheater = () => {
  return https.get("/api/QuanLyRap/LayThongTinLichChieuHeThongRap");
};
