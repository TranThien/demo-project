import { https } from "./configURL";

//cách 1 ;
// export const userService = {
//   postLogin: () => {
//     return https.post("/api/QuanLyNguoiDung/DangNhap");
//   },
// };
//c2 :
// add tài khoản đăng nhập
export const postLogin = (data) => {
  return https.post("/api/QuanLyNguoiDung/DangNhap", data);
};
